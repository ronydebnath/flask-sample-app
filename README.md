# Docker Containerized Flask App
simple Flask app that returns a json response

```json
{
  "hello": "world"
}
```

# Introduction

I've used `slim-buster` based python image as a base for this project along with postgres 12 as Database. To persist the data beyond the life of the container I've configured a volume. This config will bind postgres_data to the "/var/lib/postgresql/data/" directory in the container. 

I will be using Django's built in server in development environment and Gunicorn along with Nginx for the production environment to serve the content.

### Development


1. Rename *.env.dev-sample* to *.env.dev*.
2. Update the environment variables in the *docker-compose.yml* and *.env.dev* files(optional).
3. Build the images and run the containers:

    ```sh
    $ docker-compose up -d --build
    ```

    Test it out at [http://localhost:5000](http://localhost:5000). The "web" folder is mounted into the container and your code changes apply automatically.

### Production


1. Rename *.env.prod-sample* to *.env.prod* and *.env.prod.db-sample* to *.env.prod.db*. Update the environment variables(optional).
2. Build the images and run the containers:

    ```sh
    $ docker-compose -f docker-compose.prod.yml up -d --build
    ```

    Test it out at [http://localhost:82](http://localhost:82). Since this is a production environment, there are no mounted folders. To apply changes, the image must be re-built to see any changes.